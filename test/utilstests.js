
const utils = require('../utils')
const assert = require('assert')

const isEmpty = utils.isEmpty


describe('creating and fetching: ', ()=>{
    
    let queryMApper, queryMapperOnEmpty
    
    beforeEach(() => {
        queryMApper = {}
        queryMapperOnEmpty = {}
    })
    
    it("should return {} if query wasn't sent earlier", ()=> {
        let resp = utils.fetchQuery(queryMApper, queryMapperOnEmpty, '{}')
        assert.deepEqual({}, {})
    })

    it('should return default value if given before and queryMapper[query] is empty', ()=>{
        let query = {a:1}
        let response = {b:2}
        let onEmpty = {empty: true}
        utils.addQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query), response, onEmpty)
        //ignore response:
        utils.fetchQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query))
        let resp = utils.fetchQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query))
        assert.deepEqual(resp, onEmpty)
    })

    it('should return the response provided before when fetching', ()=>{
        let query = {a:1}, response = {b:2}
        utils.addQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query), response)
        let resp = utils.fetchQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query))
        assert.deepEqual(resp, response)
    })

    it('should return the responses in the same order of their creation', ()=>{
        let query = {a:1}
        let response1 = {a:1}, response2 = {b:2}, response3 = {c:3}
        utils.addQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query), response1)
        utils.addQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query), response2)
        utils.addQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query), response3)
        let resp1 = utils.fetchQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query))
        let resp2 = utils.fetchQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query))
        let resp3 = utils.fetchQuery(queryMApper, queryMapperOnEmpty, JSON.stringify(query))
        assert.deepEqual(resp1, response1)
        assert.deepEqual(resp2, response2)
        assert.deepEqual(resp3, response3)
    })
})

describe('validating request structure:', ()=>{
    
    it("should return true if request is in the right structure", ()=> {
        let request = {a:[], b:"b", c:[1,2,3]}
        let mandatoryFields = ['a', 'b', 'c']
        let isValid = utils.isRequestDataValid(request, mandatoryFields)
        assert.equal(isValid, true)
    })

    it("should return false if request isn't in the right structure", ()=> {
        let request = {a:[], d:"b", c:[1,2,3]}
        let mandatoryFields = ['a', 'b', 'c']
        let isValid = utils.isRequestDataValid(request, mandatoryFields)
        assert.equal(isValid, false)
    })

    it("should allow partial structure validation", ()=> {
        let request = {a:[], d:"b", c:[1,2,3]}
        let mandatoryFields = ['d']
        let isValid = utils.isRequestDataValid(request, mandatoryFields)
        assert.equal(isValid, true)
    })

    it("should return false if request is not an object", ()=> {
        let request = 'Not An Object'
        let mandatoryFields = ['0'] // a valid index for strings 
        let isValid = utils.isRequestDataValid(request, mandatoryFields)
        assert.equal(isValid, false)
    })
 
})