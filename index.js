const express = require('express')
const utils = require('./utils')
const { logger } = require('./logger')

const PORT = process.env.PORT || 3000
const CREATE_QUERY_ENDPOINT = "/createQuery"
const HTTP_BAD_REQUEST = 400
const HTTP_BAD_DATA_TYPE = 415

let MOCK_ENDPOINT = process.env.MOCK_ENDPOINT
MOCK_ENDPOINT = MOCK_ENDPOINT ? MOCK_ENDPOINT : "/v1/graphql"

let queryMapper = {}
let queryMapperOnEmpty = {}

let app = express()

app.use(express.json({limit:'10mb', extended: true}))

app.use((req,res,next)=> {
    logger.info({method: req.method, body: req.body})
    next()
})

app.use((req,res,next)=>{
    if ( req.headers["content-type"] != "application/json"){
        res.status(HTTP_BAD_DATA_TYPE)
        res.send("Wrong Content-Type. Expected: application/json. Got: " + req.headers["content-type"])
    }
    else {
        next()
    }
})

app.post(CREATE_QUERY_ENDPOINT, (req, res) => {
    if (!utils.isRequestDataValid(req.body, ['query', 'responses'])){
        res.status(HTTP_BAD_REQUEST)
        res.send("Expected fields ['query', 'responses'] in request body")
        return
    }
    let futureRequest = JSON.stringify(req.body.query)
    let expectedResponses = req.body.responses
    let onEmptyResponse = req.body.onEmptyResponse
    utils.addQuery(queryMapper, queryMapperOnEmpty, futureRequest, expectedResponses, onEmptyResponse)
    res.send("OK")
})

app.post(MOCK_ENDPOINT, (req, res) =>{
    // if (!utils.isRequestDataValid(req.body, ['query'])){
    //     res.status(HTTP_BAD_REQUEST)
    //     res.send("Expected fields ['query'] in request body")
    // }
    let mockRequest = JSON.stringify(req.body)
    let mockResponse = utils.fetchQuery(queryMapper, queryMapperOnEmpty, mockRequest)
    if (!utils.isEmpty(mockResponse)){
        logger.info({response: mockResponse})
    }
    res.set('Content-Type', 'application/json')
    res.send(mockResponse)
})

app.listen(3000, () => {console.log("Listening on port 3000")})