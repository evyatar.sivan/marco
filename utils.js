module.exports = {
    isEmpty: (obj)=>{
        return !Object.keys(obj).length
    },
    isRequestDataValid: (reqBody, expectedFields) => {
        if (typeof reqBody != 'object') {
            return false
        }
        for( var i = 0; i<expectedFields.length; i++ ){
            let field = expectedFields[i]
            if( reqBody[field] === undefined ){
                return false
            }
        }
        return true
    },
    addQuery: (queryMapper, queryMapperOnEmpty, futureRequest, expectedResponses, onEmptyResponse) => {
        if (onEmptyResponse) {
            queryMapperOnEmpty[futureRequest] = onEmptyResponse
        }
        if (!queryMapper[futureRequest]) {
            queryMapper[futureRequest] = []
        }
        expectedResponses.forEach( response => {
            queryMapper[futureRequest].push(response)
        })
    },
    setDefaultResponse: (queryMapperOnEmpty, futureRequest, onEmptyResponse) => {
        queryMapperOnEmpty[futureRequest] = onEmptyResponse
    },
    fetchQuery: (queryMapper, queryMapperOnEmpty, request) => {
        if (!queryMapper[request]) {
            return {}
        }
        else if (queryMapper[request].length === 0 ){
            if (!queryMapperOnEmpty[request]){
                return {}
            }
            return queryMapperOnEmpty[request]
        }        
        else {
            return queryMapper[request].shift()
        }
    }
}