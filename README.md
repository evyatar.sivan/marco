# Http testing mock

### What is this tool?

This tool enables to define reponses for predefined queries on a specific endpoint.

In other words, you can create a generic http server mock.

### How To use

start the project using  
```
npm install
node index.js
```

#### Set the next responses for a query

###### for example:
send POST request to /createQuery  
with content-type header: application/json
body:  
```
{
    "query": "query getUsers{ users {\n    id\n    name\n }}",
    "responses":[
        {"users": [{"name": "u1", "id": "1"}]},
        {"users": [{"name": "renamedU1", "id": "1"}]}
    ],
    "onEmptyResponse": "{}"
}
```

this means that the response for the next POST request with body  
>{"query": "query getUsers{ users {\n    id\n    name\n }}"

will be:
>{"users": [{"name": "u1", "id": "1"}]}

and the next response to the same POST request will be:
>{"users": [{"name": "renamedU1", "id": "1"}]}

##### After a response is sent it won't return again:
There is a stack of responses per query and for each response sent, the stack pops out that response.  
in case there is no response in the stack a default reponse can be defined using "onEmptyResponse" field in the createQuery request
