
const log4js = require('log4js')

const loglevel = process.env.LOG_LEVEL || 'info'

log4js.configure({
    appenders: { accessLog: {type: 'file', filename: 'access.log'} },
    categories: { default: { appenders: ['accessLog'], level: loglevel} }
})

module.exports = {
    logger: log4js.getLogger('accessLog')
}